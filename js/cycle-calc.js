const cycleFirstDateInput = $('#cycle-calc-first-input');
const cycleNumDaysInput = $('#cycle-calc-days-input');
const cycleTotalDaysInput = $('#cycle-calc-total-input');

const cycleCalendar = $("#cycle-calc-calendar");

const cycleErrorsList = $('#cycle-calc-errors-list');
const cycleHeightError = $('#cycle-calc-height-error');
const cycleWeightError = $('#cycle-calc-weight-error');
const cycleAgeError = $('#cycle-calc-age-error');
const cycleGenderError = $('#cycle-calc-gender-error');
const cycleActivitiesError = $('#cycle-calc-activities-error');

const cycleSubmitButton = $('#btn_Submit_cycleCalc_Right');
const cycleResultButton = $('#cycle-calc-result-return-button');

const cycleDates = [];

const cycleOvulDates = [];

const cycleRefreshTime = 100;

const cycleCalcInit = () => {
    $('[data-toggle="tooltip"]').popover();

    initCycleCalendar();

    $(window).resize(() => {
        initCycleCalendar();
        styleCalendar();
    });

    cycleFirstDateInput.val(moment().format('YYYY-MM-DD'));
};

const initCycleCalendar = () => {
    const windowWidth = $(window).width();

    cycleCalendar.datepicker({
        onChangeMonthYear: () => {
            setTimeout(() => styleCalendar(), cycleRefreshTime);
        }
    });

    cycleCalendar.datepicker("option", "numberOfMonths", windowWidth >= 480 ? 2 : 1);
};

function getDateStatus(date) {
    for (let i = 0; i < cycleDates.length; i++) {
        if (cycleDates[i].from <= date && cycleDates[i].to >= date) {
            return 'cycle';
        }
    }

    for (let i = 0; i < cycleOvulDates.length; i++) {
        if (cycleOvulDates[i].from <= date && cycleOvulDates[i].to >= date) {
            return 'ovul';
        }
    }

    return 'none';
}

const styleCalendar = () => {
    $(".ui-datepicker-calendar a").each(function() {
        const _this = $(this);
        const parent = _this.parent();
        const month = Number(parent.data('month'));
        const year = Number(parent.data('year'));
        const day = Number(_this.text());

        const date = new Date(year, month, day);
        const dateStatus = getDateStatus(date);
        let cls = '';

        if (dateStatus === 'cycle') {
            cls = 'cycle-day';
        } else if (dateStatus === 'ovul') {
            cls = 'ovul-day';
        }

        _this.addClass(cls);
    });
}

cycleSubmitButton.click(() => {
    const firstDate = cycleFirstDateInput.val();
    const numDays = cycleNumDaysInput.val();
    const totalDays = cycleTotalDaysInput.val();

    let nextStartDate = moment(firstDate).add(totalDays, 'days');
    let nextEndDate = moment(nextStartDate).add(numDays - 1, 'days');

    let ovulEndDate = moment(nextStartDate).subtract(12, 'days');
    let ovulStartDate = moment(ovulEndDate).subtract(4, 'days');

    addCycleDates(nextStartDate, nextEndDate);
    addCycleOvulDates(ovulStartDate, ovulEndDate);

    addPreviousCycleDates(nextStartDate, nextEndDate, totalDays, 2);
    addFutureCycleDates(nextStartDate, nextEndDate, totalDays, 15);

    addPreviouscycleOvulDates(ovulStartDate, ovulEndDate, totalDays, 2);
    addFutureCycleOvulDates(ovulStartDate, ovulEndDate, totalDays, 15);

    styleCalendar();
});

const addPreviousCycleDates = (startDate, endDate, totalDays, numMonths) => {
    if(numMonths <= 0) return;

    const newStartDate = startDate.subtract(totalDays, 'days');
    const newEndDate = endDate.subtract(totalDays, 'days');

    addCycleDates(newStartDate, newEndDate);

    addPreviousCycleDates(newStartDate, newEndDate, totalDays, numMonths - 1);

    cycleShowResults();
};

const addFutureCycleDates = (startDate, endDate, totalDays, numMonths, currentMonth = 1) => {
    if(currentMonth >= numMonths) return;

    const newStartDate = startDate.add(totalDays, 'days');
    const newEndDate = endDate.add(totalDays, 'days');

    addCycleDates(newStartDate, newEndDate);

    addFutureCycleDates(newStartDate, newEndDate, totalDays, numMonths, currentMonth + 1);

    cycleShowResults();
};

const addPreviouscycleOvulDates = (startDate, endDate, totalDays, numMonths) => {
    if(numMonths <= 0) return;

    const newStartDate = startDate.subtract(totalDays, 'days');
    const newEndDate = endDate.subtract(totalDays, 'days');

    addCycleOvulDates(newStartDate, newEndDate);

    addPreviouscycleOvulDates(newStartDate, newEndDate, totalDays, numMonths - 1);

    cycleShowResults();
};

const addFutureCycleOvulDates = (startDate, endDate, totalDays, numMonths, currentMonth = 1) => {
    if(currentMonth >= numMonths) return;

    const newStartDate = startDate.add(totalDays, 'days');
    const newEndDate = endDate.add(totalDays, 'days');

    addCycleOvulDates(newStartDate, newEndDate);

    addFutureCycleOvulDates(newStartDate, newEndDate, totalDays, numMonths, currentMonth + 1);

    cycleShowResults();
};

const addCycleDates = (startDate, endDate) => {
    cycleDates.push({
        from: new Date(startDate.format('MM/DD/YYYY')),
        to: new Date(endDate.format('MM/DD/YYYY'))
    });
};

const addCycleOvulDates = (startDate, endDate) => {
    cycleOvulDates.push({
        from: new Date(startDate.format('MM/DD/YYYY')),
        to: new Date(endDate.format('MM/DD/YYYY'))
    });
};

const cycleShowErrors = errors => {
    cycleHideErrors();

    errors.forEach(error => {
        cycleErrorsList.append(`<li class='alert-danger cycle-calc-error-item'>${error}</li>`);
    });
};

const cycleHideErrors = () => cycleErrorsList.empty();

cycleResultButton.click(() => {
    cycleHideResults();
});

const cycleHideResults = () => {
    $('.cycle-calc-result').css('display', 'none');
    $('.calcWzDescCnts').css('display', 'block');
};

const cycleShowResults = () => {
    $('.cycle-calc-result').css('display', 'flex');
    $('.calcWzDescCnts').css('display', 'none');
};